$(".center").slick({
    slidesToScroll: 1,
    dots: true,
    centerMode: true,
    centerPadding: "60px",
    slidesToShow: 3,
    arrows: true,
    responsive: [{
            breakpoint: 1500,
            settings: {
                centerMode: true,
                dots: true,
                centerPadding: "40px",
                slidesToShow: 3
            }
        },
        {
            breakpoint: 860,
            settings: {
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
    ]
});
$(".multiple-items").slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    responsive: [{
        breakpoint: 801,
        settings: {
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true
        }
    }, ]
});
$(".icon-menu").click(function() {
    $(".menu").animate({
        left: "0px"
    }, 600);
});
$(".woman").click(function() {
    $(".menu").animate({
        left: "-50%"
    }, 600);
});
$(document).ready(function() {
    var position = [22.554896, 113.887253];

    function showGoogleMaps() {
        var latLng = new google.maps.LatLng(position[0], position[1]);
        var mapOptions = {
            zoom: 16,
            streetViewControl: false,
            scaleControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: latLng
        };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP
        });
    }
    google.maps.event.addDomListener(window, "load", showGoogleMaps);
});
$(document).ready(click());
